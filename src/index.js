import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { IoIosArrowUp, IoIosArrowDown} from "react-icons/io";


function Square(props) {
    let className = "square";
    if (props.highLightWinner) {
        className += " winner";
    }

    return (
        <button className={className} onClick={props.onClick}>
            {props.value}
        </button>
    );
}

class Board extends React.Component {
    render() {
        let squares = [];

        for (let row = 0; row < 3; row++) {
            let squaresInRow = [];
            for (let col = 0; col < 3; col++) {
                const squareIndex = (row * 3 + col);
                squaresInRow.push(<Square key={squareIndex}
                                          value={this.props.squares[ squareIndex]}
                                          highLightWinner={this.props.winningSquares.includes(squareIndex)}
                                          onClick={() => this.props.onClick( squareIndex)}/>);
            }
            squares.push(<div className="board-row" key={row}>{squaresInRow}</div>)
        }

        return (
            <div>
                {squares}
            </div>
        );
    }
}

class Game extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            history: [{
                squares: Array(9).fill(null),
                row: null,
                col: null
            }],
            stepNumber: 0,
            xIsNext: true,
            highlightStep: null,
            sortHistoryAsc: true
        }
    }

    jumpTo(step) {
        this.setState({
            stepNumber: step,
            xIsNext: (step % 2) === 0,
            highlightStep: step
        });
    }

    toggleHistorySort() {
        this.setState({
            sortHistoryAsc: !this.state.sortHistoryAsc
        })

    }


    handleClick(i) {
        const history = this.state.history.slice(0, this.state.stepNumber + 1);
        const current = history[history.length - 1];
        const squares = current.squares.slice();

        if (calculateWinner(squares).winner || squares[i]) {
            return;
        }

        squares[i] = this.state.xIsNext ? 'X' : 'O';
        this.setState({
            history: history.concat([{
                squares: squares,
                row: Math.floor(i / 3) + 1,
                col: i % 3
            }]),
            stepNumber: history.length,
            xIsNext: !this.state.xIsNext
        });
    }

    render() {
        const history = this.state.history.slice();
        const current = history[this.state.stepNumber];
        const winner = calculateWinner(current.squares);

        const moves = history.map((step, move) => {
            const desc = move ? 'Go to move #' + move + '. (' + step.row + ',' + step.col + ')' : 'Go to game start';
            return (
                <li key={move} value={move + 1}>
                    <button onClick={() => this.jumpTo(move)}
                            className={this.state.highlightStep === move ? 'highlight': ''}>{desc}</button>
                </li>
            );
        });

        let sortButton = <IoIosArrowUp />;

        if (!this.state.sortHistoryAsc) {
            moves.reverse();
            sortButton = <IoIosArrowDown />;
        }

        let status;
        if (winner.winner) {
            status = 'Winner: ' + winner.winner + '. Winning squares: ' + winner.winningSquares;
        } else if(this.state.stepNumber === 9) {
            status = 'Draw';
        } else {
            status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
        }

        return (
            <div className="game">
                <div className="game-board">
                    <Board
                        squares={current.squares}
                        winningSquares={winner.winningSquares}
                        onClick={(i) => this.handleClick(i)}
                        />

                </div>
                <div className="game-info">
                    <div>{status}</div>
                    <button onClick={() => this.toggleHistorySort()}>{sortButton}</button>
                    <ol>{moves}</ol>
                </div>
            </div>
        );
    }
}

// ========================================

ReactDOM.render(
    <Game />,
    document.getElementById('root')
);

function calculateWinner(squares) {
    const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6]
    ];
    for (let i = 0; i < lines.length; i++) {
        const [a, b, c] = lines[i];
        if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
            return {
                winner: squares[a],
                winningSquares: lines[i]
            };
        }
    }
    return {winner: null, winningSquares: []};
}
